# think-sms
The ThinkPHP5 sms

## 安装

### 一、执行命令安装
```
composer require laoqianjunzi/sms
```

或者

### 二、安装

#####  安装
```
"require": {
        "laoqianjunzi/sms":"1.*"
},
```



## 使用
#### 添加配置文件
```
return [
   
];
```

#### 使用方法
```
$mobile='188888888888';
$content='短信内容';
$data=   app()->make(EmaySms::class)->sendSMS($mobile,$content);
```

     


